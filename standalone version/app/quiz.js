// Get DOM elements
const loginForm = document.getElementById("login-form");
const categoryForm = document.getElementById("category-form");
const questionForm = document.getElementById("question-form");
const greeting = document.getElementById("greeting");
const questionText = document.getElementById("question");
const answerOptions = document.getElementById("answer-options");

const restartBtn = document.getElementById("restart-btn");
const exitBtn = document.getElementById("exit-btn");
const endGameForm = document.getElementById("end-game-form");
const msgEndGame = document.getElementById("h1EndGame");
const msgCategory =  document.getElementById("msg-category");
// number of question per round of the Quiz
const nbQuestions = 10;

// initialization variable
let username = "";
let nickname = "";
let currentQuestionIndex = 0;
let selectedAnswerIndex = -1;
let nbCorrectAnswer =0;
let selectedCategory = "";

let questions = [];
let dataQuiz = [];
let categoriesQuestions = [];
let categoryChosen = "";


// Add event listeners
loginForm.addEventListener("submit", login);

// action to do when we click on login buton
function login(event) {
  event.preventDefault();
  username = document.getElementById("username").value;
  nickname = document.getElementById("nickname").value;

  // hide the login form
  loginForm.hidden = true;
  // reveal the category form
  categoryForm.hidden =  false;
  msgCategory.textContent = `Please Choose a category ${nickname}`;
}

function restart() {
  loginForm.hidden = true;
  endGameForm.hidden = true;
  questionForm.hidden = true;
  categoryForm.hidden = false;
}
// Function to start quiz
function startQuiz() {
  currentQuestionIndex = 0;
  selectedAnswerIndex = -1;
  nbCorrectAnswer =0;

  loginForm.hidden = true;
  questionForm.hidden = false;
  categoryForm.hidden =  true
  endGameForm.hidden = true;
  greeting.textContent = `Welcome, ${username}!`;
  displayQuestion();
}

function displayQuestion() {
  const currentQuestion = questions[currentQuestionIndex];
  questionText.textContent = currentQuestion.question;
  answerOptions.innerHTML = "";
  // add a button for each options of the question
  currentQuestion.options.forEach((option, index) => {
    const answerButton = document.createElement("button");
    answerButton.classList.add("options")
    answerButton.textContent = option;
    answerButton.addEventListener("click", () => {
      selectAnswer(index);
    });
    answerOptions.appendChild(answerButton);
  });
}


function selectAnswer(index) {
  selectedAnswerIndex = index;
  const answerButtons = answerOptions.getElementsByTagName("button");
  Array.from(answerButtons).forEach((button, i) => {
    button.classList.remove("selected", "correct", "incorrect");
    if (i === index) {
      button.classList.add("selected");
    }
  });
}


function submitAnswer(event) {
  event.preventDefault();
  const currentQuestion = questions[currentQuestionIndex];
  const answerButtons = answerOptions.getElementsByTagName("button");
  Array.from(answerButtons).forEach((button, i) => {
    button.disabled = true;
    if (button.textContent === currentQuestion.answer) {
      button.classList.add("correct");
      if (button.classList.contains("selected")) {
        nbCorrectAnswer++;
      }
    } else if (i === selectedAnswerIndex) {
      button.classList.add("incorrect");
    }
  });
}

function nextQuestion(){
  currentQuestionIndex++;
  if (currentQuestionIndex < questions.length) {
    setTimeout(() => {
      displayQuestion();
    }, 0);
  } else {
    questionForm.hidden = true;
    showEndGameForm();
    greeting.textContent = `Quiz complete! "Result : " + nbCorrectAnswer +"/" + nbQuestion +"."`;
  }
}

function exitGame(){
  msgEndGame.innerHTML = `Goodbye  ${username} ! `;
  exitBtn.hidden = true;
  restartBtn.hidden = true;
}

// Function to show end game form
function showEndGameForm() {
  // Hide question form
  questionForm.hidden = true;
  // Show end game form
  endGameForm.hidden = false;
  // Display score
  msgEndGame.innerHTML = `Hey ${username},  Your score is ${nbCorrectAnswer}/${nbQuestions}.`;
}


// Load json data 
function fetchData(callback) {
  fetch('data_quiz.json')
    .then(response => response.json())
    .then(data => {
      dataQuiz = data
      categoriesQuestions = Object.keys(data);
      callback({ dataQuiz, categoriesQuestions });
    })
    .catch(error => console.error(error));
}

fetchData(data => {
  const buttonContainer = document.getElementById('categories');

  // A - Create a button in the category form for each category present in the json data file
  categoriesQuestions.forEach(category => {
  const button = document.createElement('button');
  button.textContent = category;
  button.classList.add("item")
  button.setAttribute("type","button");
  buttonContainer.appendChild(button);

  // set the action when we click on a button corresponding to a category
  button.addEventListener("click", () => 
      {
        // first deselect all the other categories 
        let buttons = document.getElementsByClassName("item")
        Array.from(buttons).forEach((btn) => {
          btn.classList.remove("selected")
          })
        // then select the category of the button selected
        button.classList.add("selected")
        categoryChosen = button.textContent;
        console.log(`The category ${categoryChosen} has been selected ! `)
      });
  });

  // action to do when change occurs in the category form
  function handleParameter(event) {
    event.preventDefault();
    //save the category
    const selectElement = document.getElementById("category-form");
    selectElement.addEventListener("change", (event) => {
      categoryChosen = event.target.value;
      console.log("The Category active is " + categoryChosen);
    });
  }

  // function to randomly select n elements of an array 
  function selectRandomNValues(array, n) {
    const shuffled = array.sort(() => 0.5 - Math.random());
    return shuffled.slice(0, n);
  }

  let startQuizBtn = document.getElementById("sQuiz");
  startQuizBtn.addEventListener("click",function () {
    console.log(`The category ${categoryChosen} is confirmed ! `)
    categoryForm.hidden =  true;
    console.log("The chosen category is " + categoryChosen);
    // define te questions randomly selected from the category chosen
    questions = selectRandomNValues(dataQuiz[categoryChosen], nbQuestions);
    // launch Quiz
    startQuiz();
  });

});

