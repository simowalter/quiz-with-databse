// Get DOM elements
const loginForm = document.getElementById("login-form");
const questionForm = document.getElementById("question-form");
const greeting = document.getElementById("greeting");
const questionText = document.getElementById("question");
const answerOptions = document.getElementById("answer-options");

const restartBtn = document.getElementById("restart-btn");
const exitBtn = document.getElementById("exit-btn");
const endGameForm = document.getElementById("end-game-form");
const msgEndGame = document.getElementById("h1EndGame");

// initialization variable
let username = "";
let password = "";
let currentQuestionIndex = 0;
let selectedAnswerIndex = -1;
let nbCorrectAnswer =0;
let nbQuestion =0;

// Set up initial state
const USERNAME = "a";
const PASSWORD = "a";

// Add event listeners
loginForm.addEventListener("submit", login);

// MOC question
const questions = [
  {
    question: "What is the capital of France?",
    answers: ["London", "Madrid", "Paris", "Berlin"],
    correctAnswer: 2,
  },
  {
    question: "What is the largest planet in our solar system?",
    answers: ["Earth", "Jupiter", "Mars", "Saturn"],
    correctAnswer: 1,
  },
  {
    question: "What is the smallest planet in our solar system?",
    answers: ["Mars", "Mercury", "Venus", "Earth"],
    correctAnswer: 1,
  },
  {
    question: "Who invented the telephone?",
    answers: ["Alexander Graham Bell", "Thomas Edison", "Nikola Tesla", "Guglielmo Marconi"],
    correctAnswer: 0,
  },
  {
    question: "What is the symbol for potassium?",
    answers: ["P", "K", "Pt", "Po"],
    correctAnswer: 1,
  },
  {
    question: "What is the highest mountain in the world?",
    answers: ["Mount Everest", "Mount Kilimanjaro", "Mount McKinley", "Mount Fuji"],
    correctAnswer: 0,
  },
];

// Function to start quiz
function startQuiz() {
  currentQuestionIndex = 0;
  selectedAnswerIndex = -1;
  nbCorrectAnswer =0;
  nbQuestion =0;

  loginForm.hidden = true;
  questionForm.hidden = false;
  endGameForm.hidden = true;
  greeting.textContent = `Welcome, ${username}!`;
  displayQuestion();
}

// Define functions
function login(event) {
  event.preventDefault();
  username = document.getElementById("username").value;
  password = document.getElementById("password").value;
  if (username === USERNAME && password === PASSWORD) {
    loginForm.hidden = true;
    questionForm.hidden = false;
    greeting.textContent = `Welcome, ${username}!`;
    displayQuestion();
  } else {
    alert("Invalid username or password. Please try again.");
  }
}

function displayQuestion() {
  const currentQuestion = questions[currentQuestionIndex];
  questionText.textContent = currentQuestion.question;
  answerOptions.innerHTML = "";
  currentQuestion.answers.forEach((answer, index) => {
    const answerButton = document.createElement("button");
    answerButton.classList.add("options")
    answerButton.textContent = answer;
    answerButton.addEventListener("click", () => {
      selectAnswer(index);
    });
    answerOptions.appendChild(answerButton);
  });
}

function selectAnswer(index) {
  selectedAnswerIndex = index;
  const answerButtons = answerOptions.getElementsByTagName("button");
  Array.from(answerButtons).forEach((button, i) => {
    button.classList.remove("selected", "correct", "incorrect");
    if (i === index) {
      button.classList.add("selected");
    }
  });
}

function submitAnswer(event) {
  event.preventDefault();
  const currentQuestion = questions[currentQuestionIndex];
  const answerButtons = answerOptions.getElementsByTagName("button");
  Array.from(answerButtons).forEach((button, i) => {
    button.disabled = true;
    if (i === currentQuestion.correctAnswer) {
      button.classList.add("correct");
      if (button.classList.contains("selected")) {
        nbCorrectAnswer++;
      }
    } else if (i === selectedAnswerIndex) {
      button.classList.add("incorrect");
    }
  });
}

function nextQuestion(){
  currentQuestionIndex++;
  nbQuestion++;
  if (currentQuestionIndex < questions.length) {
    setTimeout(() => {
      displayQuestion();
    }, 0);
  } else {
    questionForm.hidden = true;
    showEndGameForm();
    greeting.textContent = `Quiz complete! "Result : " + nbCorrectAnswer +"/" + nbQuestion +"."`;
  }
}

function exitGame(){
  msgEndGame.innerHTML = `Goodbye  ${username} ! `;
  exitBtn.hidden = true;
  restartBtn.hidden = true;
}

// Function to show end game form
function showEndGameForm() {
  // Hide question form
  questionForm.hidden = true;

  // Show end game form
  endGameForm.hidden = false;

  // Display score
  msgEndGame.innerHTML = `Hey ${username},  Your score is ${nbCorrectAnswer}/${nbQuestion}.`;
}
