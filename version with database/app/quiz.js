// Get DOM elements
const loginForm = document.getElementById("login-form");
const adminForm1 = document.getElementById("admin-form-1");
const adminForm2 = document.getElementById("admin-form-2");
const categoryForm = document.getElementById("category-form");
const questionForm = document.getElementById("question-form");
const greeting = document.getElementById("greeting");
const questionText = document.getElementById("question");
const answerOptions = document.getElementById("answer-options");

const restartBtn = document.getElementById("restart-btn");
const exitBtn = document.getElementById("exit-btn");
const endGameForm = document.getElementById("end-game-form");
const msgEndGame = document.getElementById("h1EndGame");
const msgCategory =  document.getElementById("msg-category");
// number of question per round of the Quiz
const nbQuestions = 10;

const username_admin = "admin";
// password: R...h
const password_admin = "211d077e6d687490c32b6bb2bdb9e867";

let username = "";
let password_user = "";

// initialization variable
let currentQuestionIndex = 0;
let selectedAnswerIndex = -1;
let nbCorrectAnswer =0;
let selectedCategory = "";

let questions = [];
let dataQuiz = [];
let categoriesQuestions = [];
let categoryChosen = "";

var md5 = function(d){var r = M(V(Y(X(d),8*d.length)));return r.toLowerCase()};function M(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function X(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function V(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function Y(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_}

// ************************************************ MOC  ******************************************* */

let isCategoryChosenByAdmin = false;
const userData = {
  "users":[
    {
      "username": "walter",
      "password": "841d93525b9f0960ceaf38f4fdf22e2e"
    },
    {
      "username": "anna",
      "password": "a70f9e38ff015afaa9ab0aacabee2e13"
    }
  ]
};


function login(event) {
  event.preventDefault();
  username = document.getElementById("username").value;
  password_user = md5(document.getElementById("password").value);
  if (username === username_admin && password_user === password_admin) {
    
    loginForm.hidden = true;
    adminForm1.hidden = false;
    console.log(" You are the administrator !!")
    console.log(" You should be able to add question to the database using a form and also have url access to the UI of the database.")
  } else {

    let isAuthenticated = false;

    for (let i = 0; i < userData.users.length; i++) {
      if (username === userData.users[i].username  && password_user === userData.users[i].password) {
        isAuthenticated = true;
        break;
      }
    }
    if (!isAuthenticated)  
      alert("Incorrect username or password ! You can register if you are a new player .");
    else {
      console.log("User successfully authenticated .")
      // hide the login form
      loginForm.hidden = true;
      // reveal the category form
      categoryForm.hidden =  false;
      msgCategory.textContent = `Please Choose a category ${username}`;
    }
  }
}

function register(event){
  event.preventDefault();
  username = document.getElementById("username").value;
  password_user = md5(document.getElementById("password").value);

  const newUser = { "username": username, "password" : password_user };
  userData.users.push(newUser);

  console.log(userData)
  alert("User credentials added successfully ! You can login in now .");
}


function addQuestion() {
  // hide the login form
  loginForm.hidden = true;
  // reveal the admin form 2
  adminForm1.hidden = true;
  adminForm2.hidden = false;
}

function accessDatabase(){
  window.open('https://i.pinimg.com/474x/79/62/22/79622210ff23e0b0611ad521e6b3d339--monday-face-its-monday.jpg');
}

function exitAdmin(){
  loginForm.hidden = false;
  adminForm1.hidden = true;
  adminForm2.hidden = true;
}
// *********************************************************************************************** /
function restart() {
  loginForm.hidden = true;
  endGameForm.hidden = true;
  questionForm.hidden = true;
  categoryForm.hidden = false;
}
// Function to start quiz
function startQuiz() {
  currentQuestionIndex = 0;
  selectedAnswerIndex = -1;
  nbCorrectAnswer =0;

  loginForm.hidden = true;
  questionForm.hidden = false;
  categoryForm.hidden =  true
  endGameForm.hidden = true;
  greeting.textContent = `Welcome, ${username}!`;
  displayQuestion();
}

function displayQuestion() {
  const currentQuestion = questions[currentQuestionIndex];
  questionText.textContent = currentQuestion.question;
  answerOptions.innerHTML = "";
  // add a button for each options of the question
  currentQuestion.options.forEach((option, index) => {
    const answerButton = document.createElement("button");
    answerButton.classList.add("options")
    answerButton.textContent = option;
    answerButton.addEventListener("click", () => {
      selectAnswer(index);
    });
    answerOptions.appendChild(answerButton);
  });
}


function selectAnswer(index) {
  selectedAnswerIndex = index;
  const answerButtons = answerOptions.getElementsByTagName("button");
  Array.from(answerButtons).forEach((button, i) => {
    button.classList.remove("selected", "correct", "incorrect");
    if (i === index) {
      button.classList.add("selected");
    }
  });
}


function submitAnswer(event) {
  event.preventDefault();
  const currentQuestion = questions[currentQuestionIndex];
  const answerButtons = answerOptions.getElementsByTagName("button");
  Array.from(answerButtons).forEach((button, i) => {
    button.disabled = true;
    if (button.textContent === currentQuestion.answer) {
      button.classList.add("correct");
      if (button.classList.contains("selected")) {
        nbCorrectAnswer++;
      }
    } else if (i === selectedAnswerIndex) {
      button.classList.add("incorrect");
    }
  });
}

function nextQuestion(){
  currentQuestionIndex++;
  if (currentQuestionIndex < questions.length) {
    setTimeout(() => {
      displayQuestion();
    }, 0);
  } else {
    questionForm.hidden = true;
    showEndGameForm();
    greeting.textContent = `Quiz complete! "Result : " + nbCorrectAnswer +"/" + nbQuestion +"."`;
  }
}

function exitGame(){
  msgEndGame.innerHTML = `Goodbye  ${username} ! `;
  exitBtn.hidden = true;
  restartBtn.hidden = true;
}

// Function to show end game form
function showEndGameForm() {
  // Hide question form
  questionForm.hidden = true;
  // Show end game form
  endGameForm.hidden = false;
  // Display score
  msgEndGame.innerHTML = `Hey ${username},  Your score is ${nbCorrectAnswer}/${nbQuestions}.`;
}


// Load json data 
function fetchData(callback) {
  fetch('data_quiz.json')
    .then(response => response.json())
    .then(data => {
      dataQuiz = data
      categoriesQuestions = Object.keys(data);
      callback({ dataQuiz, categoriesQuestions });
    })
    .catch(error => console.error(error));
}

fetchData(data => {
  const buttonContainer = document.getElementById('categories');

  // A - Create a button in the category form for each category present in the json data file
  categoriesQuestions.forEach(category => {
  const button = document.createElement('button');
  button.textContent = category;
  button.classList.add("item")
  button.setAttribute("type","button");
  buttonContainer.appendChild(button);

  // set the action when we click on a button corresponding to a category
  button.addEventListener("click", () => 
      {
        // first deselect all the other categories 
        let buttons = document.getElementsByClassName("item")
        Array.from(buttons).forEach((btn) => {
          btn.classList.remove("selected")
          })
        // then select the category of the button selected
        button.classList.add("selected")
        categoryChosen = button.textContent;
        console.log(`The category ${categoryChosen} has been selected ! `)
      });
  });


  // **********************************

  let buttonSS =  document.getElementById("addQToDB");
  // buttonSS.addEventListener("submit", addQuestionToDB());

  const categoryContainerAdmin = document.getElementById('categories-admin');
  let categoryChosenAdmin = "";
  // A - Create a button in the category form for each category present in the json data file
  categoriesQuestions.forEach(category => {
  const button = document.createElement('button');
  button.textContent = category;
  button.classList.add("item")
  button.setAttribute("type","button");
  categoryContainerAdmin.appendChild(button);

  // set the action when we click on a button corresponding to a category
  button.addEventListener("click", () => 
      {
        // first deselect all the other categories 
        let buttons = document.getElementsByClassName("item")
        Array.from(buttons).forEach((btn) => {
          btn.classList.remove("selected")
          })
        // then select the category of the button selected
        button.classList.add("selected")
        isCategoryChosenByAdmin = true;
        categoryChosenAdmin = button.textContent;
        console.log(`The current category chosen by the admin is ${categoryChosenAdmin} . `)
      });
  });
  
  buttonSS.addEventListener("click", (event) =>  {
    // event.preventDefault();
    let question = document.getElementById("question-admin").value;
    let answer = document.getElementById("correct-answer").value;
    let option1 = document.getElementById("option-1").value;
    let option2 = document.getElementById("option-2").value;
    let option3 = document.getElementById("option-3").value;
    let option4 = document.getElementById("option-4").value;
    
    if (!isCategoryChosenByAdmin)
      alert("You must choose a category to proceed !")
    else {
          if (answer === option1 || answer === option2 || answer === option3 || answer === option4) {
            const newQuestion = { "question": question, "options" : [option1, option2, option3, option4], "answer": answer };
            
            dataQuiz[categoryChosenAdmin].push(newQuestion);
            console.log("The update question in the chosen category is ");
            console.log(dataQuiz[categoryChosenAdmin]);
            // quiData.users.push(newUser);

            // create a dialog box with a question and two options
            const addMoreQuestion = window.confirm("The input is successfully added to the database. Do you want to add more questions ?");

            if (addMoreQuestion) {
              alert(" You can input the next question to add to the database.")
              document.getElementById("question-admin").value = ""
              document.getElementById("correct-answer").value ="";
              document.getElementById("option-1").value ="";
              document.getElementById("option-2").value ="";
              document.getElementById("option-3").value ="";
              document.getElementById("option-4").value ="";
            }
            else 
              exitAdmin();
          }
          else {
            alert("Invalid data: The correct answer should be one of the four option provided !! ")
           }
      }
    });
  //********************************* */
  // action to do when change occurs in the category form
  function handleParameter(event) {
    event.preventDefault();
    //save the category
    const selectElement = document.getElementById("category-form");
    selectElement.addEventListener("change", (event) => {
      categoryChosen = event.target.value;
      console.log("The Category active is " + categoryChosen);
    });
  }

  // function to randomly select n elements of an array 
  function selectRandomNValues(array, n) {
    const shuffled = array.sort(() => 0.5 - Math.random());
    return shuffled.slice(0, n);
  }

  let startQuizBtn = document.getElementById("sQuiz");
  startQuizBtn.addEventListener("click",function () {
    console.log(`The category ${categoryChosen} is confirmed ! `)
    categoryForm.hidden =  true;
    console.log("The chosen category is " + categoryChosen);
    // define te questions randomly selected from the category chosen
    questions = selectRandomNValues(dataQuiz[categoryChosen], nbQuestions);
    // launch Quiz
    startQuiz();
  });

});

